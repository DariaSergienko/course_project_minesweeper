﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClassLibrary;

namespace Minesweeper__
{
    /// <summary>
    /// Interaction logic for Leaderboard.xaml
    /// </summary>
    public partial class Leaderboard : Page
    {
        static Difficulty difficulty = new Difficulty();
        private Game game = new Game(difficulty);

        public static ObservableCollection<Highscore> HighscoreList
        {
            get; set;
        }=new ObservableCollection<Highscore>();
        public Leaderboard()
        {
            InitializeComponent();
            game.GetList(HighscoreList);
            game.highscore.LoadHighscoreList();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void menu_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Menu());
        }
    }
}
