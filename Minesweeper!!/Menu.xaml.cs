﻿using ClassLibrary;
using System.Windows;
using System.Windows.Controls;

namespace Minesweeper__
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        private static Difficulty diff = new Difficulty();
        private Game game = new Game(diff);
        static public string playername { get; set; }
        public Menu()
        {
            InitializeComponent();
            game.GetList(Leaderboard.HighscoreList);
            game.highscore.LoadHighscoreList();
        }

        private void Button_Play_Click(object sender, RoutedEventArgs e)
        {
            playername = TextBox_PlayerName.Text;
            if(playername.Trim() == "" || playername.Trim() == null)
            {
                playername = "MysteriousHuman";
            }
            NavigationService.Navigate(new Play());
        }

        private void Button_Leaderboard_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Leaderboard());
        }

        private void Button_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Button_Rules_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new About());
        }
    }
}
