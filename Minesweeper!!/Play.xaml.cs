﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Minesweeper__
{
    /// <summary>
    /// Interaction logic for Play.xaml
    /// </summary>
    public partial class Play : Page
    {
        #region Змінні
        
        
       
        public static Difficulty difficulty = new Difficulty();
        public Game newgame = new Game(difficulty);
        #endregion Змінні
       
        public Play()
        {
            InitializeComponent();
            newgame.GetData(myArea, remainingBombs,timer, newgame,Label_status,Menu.playername);
            newgame.GetList(Leaderboard.HighscoreList);
            newgame.timer.LoadTimer();
        }
        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Menu());
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dropdown = sender as ComboBox;

            string value = dropdown.SelectedItem as string;

            switch (value)
            {
                case "Легкий":
                    difficulty.difficultyBombs = 10;
                    difficulty.difficultyRows = 8;
                    difficulty.difficultyColumns = 8;
                    break;

                case "Середній":
                    difficulty.difficultyBombs = 30;
                    difficulty.difficultyRows = 12;
                    difficulty.difficultyColumns = 12;
                    break;

                case "Складний":
                    difficulty.difficultyBombs = 55;
                    difficulty.difficultyRows = 16;
                    difficulty.difficultyColumns = 16;
                    break;
            }
            newgame.NewGame();
        }

        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Легкий");
            data.Add("Середній");
            data.Add("Складний");

            var dropdown = sender as ComboBox;

            dropdown.ItemsSource = data;
            dropdown.SelectedIndex = 0;
        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            newgame.NewGame();
        }

        private void reset_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void hint_Click(object sender, RoutedEventArgs e)
        {
            if (!newgame.gameover)
            {
                newgame.timer.seconds += 30;

                newgame.timer.UpdateTimer();

                newgame.gameboard.MarkHint();
                newgame.DrawGrid();
            }
        }
        private void menu_Click(object sender, RoutedEventArgs e) 
        {
            this.NavigationService.Navigate(new Menu());
        }
    }
}
