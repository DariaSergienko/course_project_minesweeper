﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace ClassLibrary
{
    public class Game
    {
        private Grid grid { get; set; }
        public bool gameover { get; set; } 
        public bool gamewon { get; set; } 
        private StackPanel myArea { get; set; }
        private Label remainingBombs { get; set; }
        private Label labelstatus { get; set; }
        public Board gameboard { get; set; }
        public Highscore highscore { get; set; }
        private Difficulty difficulty { get; set; }
        public Timer timer { get; set; }  
        private string playerName { get; set; }
        public Game(Difficulty diff)
        {
            difficulty = diff;
            gameboard = new Board(difficulty.difficultyBombs, difficulty.difficultyColumns, difficulty.difficultyRows);
            gameover = false;
            gamewon = false;    
            highscore = new Highscore();
        }
        public void GetData(StackPanel Area, Label bombs, Label timerlabel, Game game,Label status,string playername)
        {
            myArea = Area;
            timer = new Timer(timerlabel, game);
            remainingBombs = bombs;
            labelstatus = status;
            playerName = playername;
        }
        public void GetList(ObservableCollection<Highscore> List)
        {
            highscore.HighscoreList = List;
        }
        private void CheckWin()
        {
            if (gameboard.accuratelyMarkedBombs == gameboard.numberOfBombs && gameboard.accuratelyMarkedBombs == gameboard.markedBombs)
            {
                if (gameboard.floodFilledCells == difficulty.difficultyColumns * difficulty.difficultyRows - difficulty.difficultyBombs)
                {
                    int score;
                    gamewon = true;
                    if (difficulty.difficultyRows == 8)
                    {
                        score = 1000 - timer.seconds;
                    }
                    else if (difficulty.difficultyRows == 12)
                    {
                        score = 1500 - timer.seconds;
                    }
                    else
                    {
                        score = 2000 - timer.seconds;
                    }
                    labelstatus.Content = $"Ви виграли ! :)  Бали : {score} ";
                    labelstatus.Visibility = Visibility.Visible;
                    highscore.ForWin(score, playerName);
                    
                }
               
            }
        } 
        public void DrawGrid() 
        {
            myArea.Children.Remove(grid); 

            grid = new Grid(); 
            grid.Name = "grid"; 
            grid.Width = 420; 
            grid.Height = 420; 
            grid.HorizontalAlignment = HorizontalAlignment.Center; 
            grid.VerticalAlignment = VerticalAlignment.Center;

            for (int i = 0; i < difficulty.difficultyColumns; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                grid.ColumnDefinitions.Add(col);
            } 

            for (int i = 0; i < difficulty.difficultyRows; i++)
            {
                RowDefinition row = new RowDefinition();
                grid.RowDefinitions.Add(row);
            } 

            for (int i = 0; i < difficulty.difficultyRows; i++)
            {
                for (int j = 0; j < difficulty.difficultyColumns; j++)
                {
                    Button button = new Button(); 
                    button.Background = new SolidColorBrush(Color.FromRgb(148, 199, 82)); 
                    button.BorderBrush = new SolidColorBrush(Color.FromRgb(203, 110, 122));
                    if (difficulty.difficultyRows == 8)
                    {
                        button.FontSize = 25;
                    }
                    else if (difficulty.difficultyRows == 12)
                    {
                        button.FontSize = 20;
                    }
                    else
                    {
                        button.FontSize = 16;
                    }
                    button.FontFamily = new FontFamily("Suplexmentary Comic NC");
                    button.Click += LeftClick; 
                    button.MouseRightButtonUp += RightClick;
                    if (gameboard.grid[i, j].isFloodFillMarked && !gameboard.grid[i, j].isBomb) 
                    {
                        button.Background = new SolidColorBrush(Color.FromRgb(255, 239, 239));

                        switch (gameboard.grid[i, j].adjacentBombs)
                        {
                            case 1:
                                button.Foreground = Brushes.CornflowerBlue;
                                break;

                            case 2:
                                button.Foreground = Brushes.DarkSeaGreen;
                                break;

                            case 3:
                                button.Foreground = Brushes.Salmon;
                                break;

                            case 4:
                                button.Foreground = Brushes.MediumPurple;
                                break;

                            case 5:
                                button.Foreground = Brushes.Brown;
                                break;

                            case 6:
                                button.Foreground = Brushes.Teal;
                                break;

                            case 7:
                                button.Foreground = Brushes.Magenta;
                                break;

                            case 8:
                                button.Foreground = Brushes.Yellow;
                                break;
                        }

                        if (gameboard.grid[i, j].adjacentBombs > 0) 
                        {
                            button.Content = gameboard.grid[i, j].adjacentBombs; 
                        }
                    } 

                    if (gameboard.grid[i, j].isFloodFillMarked && gameboard.grid[i, j].isBomb) 
                    {
                        button.Background = Brushes.PowderBlue;
                        button.Content = "\uD83D\uDCA3"; 
                        button.Foreground = Brushes.DarkSlateBlue;
                        gameover = true;
                        RevealAllGrid(); 
                        labelstatus.Content = "Гру завершено ! ;(";
                        labelstatus.Visibility = Visibility.Visible;
                    } 

                    if (!gameboard.grid[i, j].isFloodFillMarked && gameboard.grid[i, j].isHint)
                    {
                        button.Background = Brushes.CornflowerBlue;
                    } 

                    if (gameboard.grid[i, j].isBombFlagged)
                    {
                        button.Content = "\u2691";
                        button.Foreground = Brushes.IndianRed;
                    } 

                    
                    Grid.SetColumn(button, i); 
                    Grid.SetRow(button, j); 
                    grid.Children.Add(button); 
                                               
                }
            } 

            myArea.Children.Add(grid);
            remainingBombs.Content = "Бомби: " + (gameboard.numberOfBombs - gameboard.markedBombs).ToString();
        }
        private void RevealAllGrid() 
        {
            myArea.Children.Remove(grid);

            grid = new Grid();
            grid.Name = "grid";
            grid.Width = 420;
            grid.Height = 420;
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            grid.VerticalAlignment = VerticalAlignment.Center;

            for (int i = 0; i < difficulty.difficultyColumns; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                grid.ColumnDefinitions.Add(col);
            }

            for (int i = 0; i < difficulty.difficultyRows; i++)
            {
                RowDefinition row = new RowDefinition();
                grid.RowDefinitions.Add(row);
            }

            for (int i = 0; i < difficulty.difficultyRows; i++)
            {
                for (int j = 0; j < difficulty.difficultyColumns; j++)
                {
                    Button button = new Button(); 
                    button.Background = new SolidColorBrush(Color.FromRgb(148, 199, 82)); 
                    button.BorderBrush = new SolidColorBrush(Color.FromRgb(203, 110, 122));
                    if (difficulty.difficultyRows == 8)
                    {
                        button.FontSize = 25;
                    }
                    else if (difficulty.difficultyRows == 12)
                    {
                        button.FontSize = 20;
                    }
                    else
                    {
                        button.FontSize = 16;
                    }
                    button.FontFamily = new FontFamily("Suplexmentary Comic NC");

                    button.Click += LeftClick;
                    button.MouseRightButtonUp += RightClick;
                    if (gameover)
                    {
                        gameboard.Click(i, j);
                    }

                    if (gameboard.grid[i, j].isFloodFillMarked && !gameboard.grid[i, j].isBomb)
                    {
                        button.Background = new SolidColorBrush(Color.FromRgb(255, 239, 239));

                        switch (gameboard.grid[i, j].adjacentBombs)
                        {
                            case 1:
                                button.Foreground = Brushes.CornflowerBlue;
                                break;

                            case 2:
                                button.Foreground = Brushes.DarkSeaGreen;
                                break;

                            case 3:
                                button.Foreground = Brushes.Salmon;
                                break;

                            case 4:
                                button.Foreground = Brushes.MediumPurple;
                                break;

                            case 5:
                                button.Foreground = Brushes.Brown;
                                break;

                            case 6:
                                button.Foreground = Brushes.Teal;
                                break;

                            case 7:
                                button.Foreground = Brushes.Magenta;
                                break;

                            case 8:
                                button.Foreground = Brushes.Yellow;
                                break;
                        }

                        if (gameboard.grid[i, j].adjacentBombs > 0)
                        {
                            button.Content = gameboard.grid[i, j].adjacentBombs;
                        }
                    }

                    if (gameboard.grid[i, j].isFloodFillMarked && gameboard.grid[i, j].isBomb)
                    {
                        button.Background = Brushes.PowderBlue;
                        button.Content = "\uD83D\uDCA3";
                        button.Foreground = Brushes.DarkSlateBlue;
                    }

                    if (gameboard.grid[i, j].isBombFlagged)
                    {
                        button.Content = "\u2691";
                        button.Foreground = Brushes.IndianRed;
                    }

                    Grid.SetColumn(button, i);
                    Grid.SetRow(button, j);

                    grid.Children.Add(button);
                }
            }
            remainingBombs.Content = "Бомби: " + (gameboard.numberOfBombs - gameboard.markedBombs).ToString();
        }
        private void LeftClick(object sender, RoutedEventArgs e)
        {
            if (!gameover && !gamewon) 
            {
                int x = Grid.GetColumn((Button)sender); 
                int y = Grid.GetRow((Button)sender);

                if (!gameboard.grid[x, y].isBombFlagged && !gameboard.grid[x, y].isFloodFillMarked)
                {
                    
                    gameboard.Click(x, y); 
                }
                else if (!gameboard.grid[x, y].isBombFlagged && gameboard.grid[x, y].isFloodFillMarked)
                {
                    
                    gameboard.ClickNumber(x, y); 
                }
                DrawGrid(); 
                CheckWin();
            }
        }
        private void RightClick(object sender, RoutedEventArgs e)
        {
            if (!gameover && !gamewon) 
            {
                int x = Grid.GetColumn((Button)sender); 
                int y = Grid.GetRow((Button)sender);

                gameboard.RightClick(x, y); 

                DrawGrid(); 
                CheckWin(); 
            }
        }
        public void NewGame()
        {
            labelstatus.Visibility = Visibility.Hidden;
            gameboard = new Board(difficulty.difficultyRows, difficulty.difficultyColumns, difficulty.difficultyBombs);
            timer.seconds = 0;
            gameover = false;
            gamewon = false;
            DrawGrid(); 
            timer.UpdateTimer();
        }
    }
}
