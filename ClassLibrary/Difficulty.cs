﻿namespace ClassLibrary
{
    public class Difficulty
    {
        public int difficultyBombs { get; set; } 
        public int difficultyRows { get; set; } 
        public int difficultyColumns { get; set; } 
        public Difficulty()
        {
            difficultyBombs = 10;
            difficultyRows = 8;
            difficultyColumns = 8;
        }
        public Difficulty(int bombs, int rows, int columns)
        {
            difficultyBombs = bombs;
            difficultyRows = rows;
            difficultyColumns = columns;
        }
    }
}
