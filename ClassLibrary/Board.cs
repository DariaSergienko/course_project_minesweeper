﻿using System;

namespace ClassLibrary
{
    public class Board
    {
        public int numberOfBombs { get; set; } 
        public int markedBombs { get; set; } 
        public int accuratelyMarkedBombs { get; set; } 
        public int floodFilledCells { get; set; } 
        public Cell[,] grid { get; set; } 

        public Board(int rows, int columns, int bombs)
        {
            numberOfBombs = bombs;
            accuratelyMarkedBombs = 0;
            floodFilledCells = 0;
            grid = new Cell[rows, columns];

            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    grid[x, y] = new Cell();
                }
            }
            PlaceBombs();
            CalculateAdjacentBombs();
        } 
        public void MarkHint()
        {
            bool found = false;

            for (int x = 0; x < grid.GetLength(0); x++)
            {
                if (found)
                {
                    break;
                }
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    if (!grid[x, y].isBomb && !grid[x, y].isFloodFillMarked)
                    {
                        grid[x, y].isHint = true;
                        found = true;
                        break;
                    }
                }
            }
        }

        public void RightClick(int x, int y)
        {
            if (x < grid.GetLength(0) && y < grid.GetLength(1))
            {
                if (!grid[x, y].isFloodFillMarked)
                {
                    if (!grid[x, y].isBombFlagged)
                    {
                        grid[x, y].isBombFlagged = true;

                        if (grid[x, y].isBomb)
                        {
                            accuratelyMarkedBombs++;
                            markedBombs++;
                        }
                        else
                        {
                            markedBombs++;
                        }
                    }
                    else
                    {
                        grid[x, y].isBombFlagged = false;

                        if (grid[x, y].isBomb)
                        {
                            accuratelyMarkedBombs--;
                            markedBombs--;
                        }
                        else
                        {
                            markedBombs--;
                        }
                    }
                }
            }
        }
        public void Click(int x, int y)
        {
            if (x < grid.GetLength(0) && y < grid.GetLength(1))
            {
                FloodFillMarkGrid(x, y);
            }
            grid[x, y].isFloodFillMarked = true;
        }
        public void ClickNumber(int x, int y)
        {
            if (x < grid.GetLength(0) && y < grid.GetLength(1))
            {
                int flags=0;
                int right_flags = 0;
                if (x > 0 && x < grid.GetLength(0) - 1 && y > 0 && y < grid.GetLength(0) - 1)
                {
                    right_flags = CalculateFlagsCenter(x, y,ref flags);

                }
                else if (x == 0 && y == 0)
                {
                    right_flags = CalculateFlagsUpperLeftEdge(x, y, ref flags);

                }
                else if (x == 0 && y == grid.GetLength(0) - 1)
                {
                    right_flags = CalculateFlagsBottomLeftEdge(x, y, ref flags);

                }
                else if (x == grid.GetLength(0) - 1 && y == 0)
                {
                    right_flags = CalculateFlagsUpperRightEdge(x, y, ref flags);

                }
                else if (x == grid.GetLength(0) - 1 && y == grid.GetLength(0) - 1)
                {
                    right_flags = CalculateFlagsBottomRightEdge(x, y, ref flags);

                }
                else if (y > 0 && y < grid.GetLength(0) - 1 && x == 0)
                {
                    right_flags = CalculateFlagsLeftSide(x, y, ref flags);

                }
                else if (x > 0 && x < grid.GetLength(0) - 1 && y == 0)
                {
                    right_flags = CalculateFlagsUpperSide(x, y, ref flags);

                }
                else if (x == grid.GetLength(0) - 1 && y > 0 && y < grid.GetLength(0) - 1)
                {
                    right_flags = CalculateFlagsRightSide(x, y, ref flags);

                }
                else if (x > 0 && x < grid.GetLength(0) - 1 && y == grid.GetLength(0) - 1)
                {
                    right_flags = CalculateFlagsBottomSide(x, y, ref flags);

                }
                if (right_flags == grid[x, y].adjacentBombs&&flags==grid[x,y].adjacentBombs)
                {

                    FloodFillMark(x, y);
                }
            }

        }
        private int CalculateFlagsCenter(int x, int y,ref int allflags)
        {
            int flags = 0;
            if (grid[x - 1, y - 1].isBomb && grid[x - 1, y - 1].isBombFlagged) flags++;
            if (grid[x - 1, y - 1].isBombFlagged) allflags++;
            if (grid[x, y - 1].isBomb && grid[x, y - 1].isBombFlagged) flags++;
            if ( grid[x, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y - 1].isBomb && grid[x + 1, y - 1].isBombFlagged) flags++;
            if ( grid[x + 1, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y].isBomb && grid[x + 1, y].isBombFlagged) flags++;
            if (grid[x + 1, y].isBombFlagged) allflags++;
            if (grid[x + 1, y + 1].isBomb && grid[x + 1, y + 1].isBombFlagged) flags++;
            if (grid[x + 1, y + 1].isBombFlagged) allflags++;
            if (grid[x, y + 1].isBomb && grid[x, y + 1].isBombFlagged) flags++;
            if (grid[x, y + 1].isBombFlagged) allflags++;
            if (grid[x - 1, y + 1].isBomb && grid[x - 1, y + 1].isBombFlagged) flags++;
            if (grid[x - 1, y + 1].isBombFlagged) allflags++;
            if (grid[x - 1, y].isBomb && grid[x - 1, y].isBombFlagged) flags++;
            if ( grid[x - 1, y].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsUpperLeftEdge(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x, y + 1].isBomb && grid[x, y + 1].isBombFlagged) flags++;
            if (grid[x, y + 1].isBombFlagged) allflags++;
            if (grid[x + 1, y + 1].isBomb && grid[x + 1, y + 1].isBombFlagged) flags++;
            if (grid[x + 1, y + 1].isBombFlagged) allflags++;
            if (grid[x + 1, y].isBomb && grid[x + 1, y].isBombFlagged) flags++;
            if (grid[x + 1, y].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsBottomLeftEdge(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x, y - 1].isBomb && grid[x, y - 1].isBombFlagged) flags++;
            if ( grid[x, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y - 1].isBomb && grid[x + 1, y - 1].isBombFlagged) flags++;
            if (grid[x, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y].isBomb && grid[x + 1, y].isBombFlagged) flags++;
            if (grid[x + 1, y].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsUpperRightEdge(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x - 1, y].isBomb && grid[x - 1, y].isBombFlagged) flags++;
            if ( grid[x - 1, y].isBombFlagged) allflags++;
            if (grid[x - 1, y + 1].isBomb && grid[x - 1, y + 1].isBombFlagged) flags++;
            if (grid[x - 1, y + 1].isBombFlagged) allflags++;
            if (grid[x, y + 1].isBomb && grid[x, y + 1].isBombFlagged) flags++;
            if (grid[x, y + 1].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsBottomRightEdge(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x - 1, y].isBomb && grid[x - 1, y].isBombFlagged) flags++;
            if (grid[x - 1, y].isBombFlagged) allflags++;
            if (grid[x - 1, y - 1].isBomb && grid[x - 1, y - 1].isBombFlagged) flags++;
            if ( grid[x - 1, y - 1].isBombFlagged) allflags++;
            if (grid[x, y - 1].isBomb && grid[x, y - 1].isBombFlagged) flags++;
            if ( grid[x, y - 1].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsLeftSide(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x, y - 1].isBomb && grid[x, y - 1].isBombFlagged) flags++;
            if (grid[x, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y - 1].isBomb && grid[x + 1, y - 1].isBombFlagged) flags++;
            if ( grid[x + 1, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y].isBomb && grid[x + 1, y].isBombFlagged) flags++;
            if (grid[x + 1, y].isBombFlagged) allflags++;
            if (grid[x + 1, y + 1].isBomb && grid[x + 1, y + 1].isBombFlagged) flags++;
            if ( grid[x + 1, y + 1].isBombFlagged) allflags++;
            if (grid[x, y + 1].isBomb && grid[x, y + 1].isBombFlagged) flags++;
            if ( grid[x, y + 1].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsUpperSide(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x - 1, y].isBomb && grid[x - 1, y].isBombFlagged) flags++;
            if ( grid[x - 1, y].isBombFlagged) allflags++;
            if (grid[x - 1, y + 1].isBomb && grid[x - 1, y + 1].isBombFlagged) flags++;
            if (grid[x - 1, y + 1].isBombFlagged) allflags++;
            if (grid[x, y + 1].isBomb && grid[x, y + 1].isBombFlagged) flags++;
            if ( grid[x, y + 1].isBombFlagged) allflags++;
            if (grid[x + 1, y + 1].isBomb && grid[x + 1, y + 1].isBombFlagged) flags++;
            if ( grid[x + 1, y + 1].isBombFlagged) allflags++;
            if (grid[x + 1, y].isBomb && grid[x + 1, y].isBombFlagged) flags++;
            if (grid[x + 1, y].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsRightSide(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x, y - 1].isBomb && grid[x, y - 1].isBombFlagged) flags++;
            if ( grid[x, y - 1].isBombFlagged) allflags++;
            if (grid[x - 1, y - 1].isBomb && grid[x - 1, y - 1].isBombFlagged) flags++;
            if (grid[x - 1, y - 1].isBombFlagged) allflags++;
            if (grid[x - 1, y].isBomb && grid[x - 1, y].isBombFlagged) flags++;
            if (grid[x - 1, y].isBombFlagged) allflags++;
            if (grid[x - 1, y + 1].isBomb && grid[x - 1, y + 1].isBombFlagged) flags++;
            if (grid[x - 1, y + 1].isBombFlagged) allflags++;
            if (grid[x, y + 1].isBomb && grid[x, y + 1].isBombFlagged) flags++;
            if (grid[x, y + 1].isBombFlagged) allflags++;
            return flags;
        }
        private int CalculateFlagsBottomSide(int x, int y, ref int allflags)
        {
            int flags = 0;
            if (grid[x - 1, y].isBomb && grid[x - 1, y].isBombFlagged) flags++;
            if (grid[x - 1, y].isBombFlagged) allflags++;
            if (grid[x - 1, y - 1].isBomb && grid[x - 1, y - 1].isBombFlagged) flags++;
            if (grid[x - 1, y - 1].isBombFlagged) allflags++;
            if (grid[x, y - 1].isBomb && grid[x, y - 1].isBombFlagged) flags++;
            if (grid[x, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y - 1].isBomb && grid[x + 1, y - 1].isBombFlagged) flags++;
            if (grid[x + 1, y - 1].isBombFlagged) allflags++;
            if (grid[x + 1, y].isBomb && grid[x + 1, y].isBombFlagged) flags++;
            if (grid[x + 1, y].isBombFlagged) allflags++;
            return flags;
        }
        private void FloodFillMarkGrid(int x, int y)
        {
            if (!grid[x, y].isBomb && !grid[x, y].isFloodFillMarked)
            {
                grid[x, y].isFloodFillMarked = true;
                floodFilledCells += 1;
                if (grid[x, y].adjacentBombs == 0)
                {
                    FloodFillMark(x, y);
                }
            }
        }
        private void CalculateCenterFloodFillMark(int x, int y)
        {
            for (int x2 = x - 1; x2 < x + 2; x2++)
            {
                for (int y2 = y - 1; y2 < y + 2; y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateLeftEdgeFloodFillMark(int x)
        {
            for (int x2 = x - 1; x2 < x + 2; x2++)
            {
                for (int y2 = 0; y2 < 2; y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateRightEdgeFloodFillMark(int x)
        {
            for (int x2 = x - 1; x2 < x + 2; x2++)
            {
                for (int y2 = grid.GetLength(1) - 2; y2 < grid.GetLength(1); y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateTopEdgeFloodFillMark(int y)
        {
            for (int x2 = 0; x2 < 2; x2++)
            {
                for (int y2 = y - 1; y2 < y + 2; y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateTopLeftCornerFloodFillMark()
        {
            for (int x2 = 0; x2 < 2; x2++)
            {
                for (int y2 = 0; y2 < 2; y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateTopRightCornerFloodFillMark()
        {
            for (int x2 = 0; x2 < 2; x2++)
            {
                for (int y2 = grid.GetLength(1) - 2; y2 < grid.GetLength(1); y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateBottomEdgeFloodFillMark(int y)
        {
            for (int x2 = grid.GetLength(0) - 2; x2 < grid.GetLength(0); x2++)
            {
                for (int y2 = y - 1; y2 < y + 2; y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateBottomLeftCornerFloodFillMark()
        {
            for (int x2 = grid.GetLength(0) - 2; x2 < grid.GetLength(0); x2++)
            {
                for (int y2 = 0; y2 < 2; y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void CalculateBottomRightCornerFloodFillMark()
        {
            for (int x2 = grid.GetLength(0) - 2; x2 < grid.GetLength(0); x2++)
            {
                for (int y2 = grid.GetLength(1) - 2; y2 < grid.GetLength(1); y2++)
                {
                    FloodFillMarkGrid(x2, y2);
                }
            }
        }
        private void FloodFillMark(int x, int y)
        {
            if (x != 0 && x != grid.GetLength(0) - 1)
            {
                if (y != 0 && y != grid.GetLength(1) - 1)
                {
                    CalculateCenterFloodFillMark(x, y);
                }
                else if (y == 0)
                {
                    CalculateLeftEdgeFloodFillMark(x);
                }
                else
                {
                    CalculateRightEdgeFloodFillMark(x);
                }
            }
            else if (x == 0)
            {
                if (y != 0 && y != grid.GetLength(1) - 1)
                {
                    CalculateTopEdgeFloodFillMark(y);
                }
                else if (y == 0)
                {
                    CalculateTopLeftCornerFloodFillMark();
                }
                else
                {
                    CalculateTopRightCornerFloodFillMark();
                }
            }
            else
            {
                if (y != 0 && y != grid.GetLength(1) - 1)
                {
                    CalculateBottomEdgeFloodFillMark(y);
                }
                else if (y == 0)
                {
                    CalculateBottomLeftCornerFloodFillMark();
                }
                else
                {
                    CalculateBottomRightCornerFloodFillMark();
                }
            }
        }
        private void PlaceBombs()
        {
            Random bombPlacer = new Random();

            for (int bombs = 0; bombs < numberOfBombs; bombs++)
            {
                int x = bombPlacer.Next(0, grid.GetLength(0));
                int y = bombPlacer.Next(0, grid.GetLength(1));

                if (!grid[x, y].isBomb)
                {
                    grid[x, y].isBomb = true;
                }
                else
                {
                    bombs--;
                }
            }
        }
        private int CalculateCenterBombs(int x, int y)
        {
            int adjacentBombs = 0;

            for (int x2 = x - 1; x2 < x + 2; x2++)
            {
                for (int y2 = y - 1; y2 < y + 2; y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }

            return adjacentBombs;
        }
        private int CalculateLeftEdgeBombs(int x)
        {
            int adjacentBombs = 0;

            for (int x2 = x - 1; x2 < x + 2; x2++)
            {
                for (int y2 = 0; y2 < 2; y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateRightEdgeBombs(int x)
        {
            int adjacentBombs = 0;

            for (int x2 = x - 1; x2 < x + 2; x2++)
            {
                for (int y2 = grid.GetLength(1) - 2; y2 < grid.GetLength(1); y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateTopEdgeBombs(int y)
        {
            int adjacentBombs = 0;

            for (int x2 = 0; x2 < 2; x2++)
            {
                for (int y2 = y - 1; y2 < y + 2; y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateTopLeftCornerBombs()
        {
            int adjacentBombs = 0;

            for (int x2 = 0; x2 < 2; x2++)
            {
                for (int y2 = 0; y2 < 2; y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateTopRightCornerBombs()
        {
            int adjacentBombs = 0;

            for (int x2 = 0; x2 < 2; x2++)
            {
                for (int y2 = grid.GetLength(1) - 2; y2 < grid.GetLength(1); y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateBottomEdgeBombs(int y)
        {
            int adjacentBombs = 0;

            for (int x2 = grid.GetLength(0) - 2; x2 < grid.GetLength(0); x2++)
            {
                for (int y2 = y - 1; y2 < y + 2; y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateBottomLeftCornerBombs()
        {
            int adjacentBombs = 0;

            for (int x2 = grid.GetLength(0) - 2; x2 < grid.GetLength(0); x2++)
            {
                for (int y2 = 0; y2 < 2; y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateBottomRightCornerBombs()
        {
            int adjacentBombs = 0;

            for (int x2 = grid.GetLength(0) - 2; x2 < grid.GetLength(0); x2++)
            {
                for (int y2 = grid.GetLength(1) - 2; y2 < grid.GetLength(1); y2++)
                {
                    if (grid[x2, y2].isBomb)
                    {
                        adjacentBombs++;
                    }
                }
            }
            return adjacentBombs;
        }
        private int CalculateCellBombs(int x, int y)
        {
            int adjacentBombs = 0;
            if (x != 0 && x != grid.GetLength(0) - 1)
            {
                if (y != 0 && y != grid.GetLength(1) - 1)
                {
                    adjacentBombs = CalculateCenterBombs(x, y);
                }
                else if (y == 0)
                {
                    adjacentBombs = CalculateLeftEdgeBombs(x);
                }
                else
                {
                    adjacentBombs = CalculateRightEdgeBombs(x);
                }
            }
            else if (x == 0)
            {
                if (y != 0 && y != grid.GetLength(1) - 1)
                {
                    adjacentBombs = CalculateTopEdgeBombs(y);
                }
                else if (y == 0)
                {
                    adjacentBombs = CalculateTopLeftCornerBombs();
                }
                else
                {
                    adjacentBombs = CalculateTopRightCornerBombs();
                }
            }
            else
            {
                if (y != 0 && y != grid.GetLength(1) - 1)
                {
                    adjacentBombs = CalculateBottomEdgeBombs(y);
                }
                else if (y == 0)
                {
                    adjacentBombs = CalculateBottomLeftCornerBombs();
                }
                else
                {
                    adjacentBombs = CalculateBottomRightCornerBombs();
                }
            }

            return adjacentBombs;
        }
        private void CalculateAdjacentBombs()
        {
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    if (!grid[x, y].isBomb)
                    {
                        grid[x, y].adjacentBombs = CalculateCellBombs(x, y);
                    }
                }
            }
        }

    }
}
