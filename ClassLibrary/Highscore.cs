﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace ClassLibrary
{
    public class Highscore
    {
        public string PlayerName { get; set; }
        public int Score { get; set; }
        private int MaxHighscoreListEntryCount { get; set; }
        public ObservableCollection<Highscore> HighscoreList
        {
            get; set;
        }
        public Highscore()
        {
            MaxHighscoreListEntryCount = 10;
        }
        public void ForWin(int score,string playerName)
        {
            int newIndex = 0;
            if ((HighscoreList.Count > 0) && (score < HighscoreList.Max(x => x.Score)))
            {
                Highscore justAbove = HighscoreList.OrderByDescending(x => x.Score).First(x => x.Score >= score);
                if (justAbove != null)
                    newIndex = HighscoreList.IndexOf(justAbove) + 1;
            }
            HighscoreList.Insert(newIndex, new Highscore()
            {
                PlayerName = playerName,
                Score = score
            });
            while (HighscoreList.Count > MaxHighscoreListEntryCount)
                HighscoreList.RemoveAt(MaxHighscoreListEntryCount);
            SaveHighscoreList();
        }
        public void LoadHighscoreList()
        {
            if (File.Exists("minesweeper_highscorelist.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Highscore>));
                using (Stream reader = new FileStream("minesweeper_highscorelist.xml", FileMode.Open))
                {
                    List<Highscore> tempList = (List<Highscore>)serializer.Deserialize(reader);
                    HighscoreList.Clear();
                    foreach (var item in tempList.OrderByDescending(x => x.Score))
                    {
                        HighscoreList.Add(item);
                    }
                }
            }
        }
        private void SaveHighscoreList()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Highscore>));
            using (Stream writer = new FileStream("minesweeper_highscorelist.xml", FileMode.Create))
            {
                serializer.Serialize(writer, HighscoreList);
            }
        }
    }
}
