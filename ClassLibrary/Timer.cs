﻿using System;
using System.Windows.Controls;
namespace ClassLibrary
{
    public class Timer
    {
        private Label timer { get; set; }
        public int seconds { get; set; }
        private Game newgame { get; set; }
        public Timer(Label label, Game game)
        {
            newgame = game;
            timer = label;
            seconds = 0;
        }
        private void DispatchTimer_Tick(object sender, EventArgs e)
        {
            if (!newgame.gameover && !newgame.gamewon)
            {
                seconds++;
                UpdateTimer();
            }
        }
        public void LoadTimer()
        {
            System.Windows.Threading.DispatcherTimer dispatchTimer = new System.Windows.Threading.DispatcherTimer();
            dispatchTimer.Tick += DispatchTimer_Tick;
            dispatchTimer.Interval = new TimeSpan(0, 0, 1);
            dispatchTimer.Start();

        }
        public void UpdateTimer()
        {
            if (seconds % 60 < 10)
            {
                timer.Content = $"{seconds / 60}" + ":" + "0" + $"{seconds % 60}";
            }
            else
            {
                timer.Content = $"{seconds / 60}" + ":" + $"{seconds % 60}";
            }
        }
    }
}
