namespace ClassLibrary
{
    public class Cell 
    {
        public bool isBomb { get; set; } 
        public int adjacentBombs { get; set; } 
        public bool isFloodFillMarked { get; set; } 
        public bool isBombFlagged { get; set; } 
        public bool isHint { get; set; } 
        public Cell() 
        {
            isBomb = false;
            adjacentBombs = 0;
            isFloodFillMarked = false;
            isBombFlagged = false;
            isHint = false;
        }
    }
}
